







def main():
    # Calcular tempo inicial do script
    tempo_inicial = dt.datetime.now().strftime("%H:%M:%S")

    # Divide a Imagem em blocos de 8x8
    lista_img_8x8 = img_8x8(argv_img)

    # Extrai a entropia de cada bloco img_8x8
    lista_entropia_por_bloco = entropia_por_bloco(lista_img_8x8)

    # FEATURE 1 - Entropia da imagem inteira
    entropia_img_full = extract_entropy(argv_img)

    # FEATURE 2 - Media da entropia de cada bloco da imagem
    media_entropia = media_entropia_blocos(lista_entropia_por_bloco)

    # FEATURE 3 - Desvio Padrao da entropia de cada bloco da imagem
    stdev_entropia = stdev_entropia_blocos(lista_entropia_por_bloco)

    # Transformar cada bloco em Lista de Markov Chains
    matrix_markov_chains = markov_chains(lista_img_8x8)

    # Isolar variaveis do Dataframe para calculo de Média e Desvio Padrao
    mk_00, mk_01, mk_10, mk_11 = isolar_variaveis_matrix_markov_chains(matrix_markov_chains)

    # FEATURE 4-7 - Média de markov chains por cada bloco (0/0, 0/1, 1/0, 1/1).
    media_mk_00, media_mk_01, media_mk_10, media_mk_11 = media_matrix_markov(mk_00, mk_01, mk_10, mk_11)

    # FEATURE 8-11 - Desvio Padrao de markov chains por cada bloco (0/0, 0/1, 1/0, 1/1).
    stdev_mk_00, stdev_mk_01, stdev_mk_10, stdev_mk_11 = stdev_matrix_markov(mk_00, mk_01, mk_10, mk_11)

    # FEATURE 12-15 - "The average probability" por toda a imagem para cada uma das
    #                   transições (0/0, 0/1, 1/0, 1/1).

    # FEATURE 16-51 - The conditional entropy for each non-boundary
    #                 position in the 8 x 8 DCT coefficient grid, calculated for
    #                 the entire image.


    print(entropia_img_full)
    print(media_entropia)
    print(stdev_entropia)
    print("MÉDIA")
    print(media_mk_00)
    print(media_mk_01)
    print(media_mk_10)
    print(media_mk_11)
    print("FIM MEDIA")
    print(stdev_mk_00)
    print(stdev_mk_01)
    print(stdev_mk_10)
    print(stdev_mk_11)

    print("--- Tempo Inicial de Execução:   "+ tempo_inicial +" ---")
    tempo_final = dt.datetime.now().strftime("%H:%M:%S")
    print("--- Tempo Final de Execução:     "+ tempo_final  + " ---")
