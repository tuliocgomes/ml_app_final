# -*- coding: utf-8 -*-

from skimage import io, color, img_as_ubyte
from sklearn.metrics.cluster import entropy
import os, sys
import numpy as np
from PIL import Image
import statistics
import pandas as pd
import datetime as dt

# Argumento que passa o nome da imagem
#argv_img = sys.arv[1]
<<<<<<< HEAD
argv_img = "./ml_app_final/test.jpg"
=======
argv_img = "test2.jpg"
>>>>>>> b2e787ea34ef20ac7fef430217874e6dbfd24070
#argv_img = 'test.jpg'

# Variaveis de features
entropia_por_bloco = []
lista_blocos = []
bw, bh = 8, 8 # tamanho bloco (altura e largura)

def open_img(path_img):
    # Por duvida sobre qual metodo usar para abrir a imagem, deixarei os dois aqui
    # ==========================================================================
    # image_gray = imread(argv_img, as_gray=True)
    # print(image_gray)
    # .shape confere as dimensoes das matrizes
    # print(image_gray.shape)
    # Abre a img com o seguinte padrao:
    # [[0.43132667 0.43132667 0.43132667 ... 0.46391176 0.46783333 0.46783333]
    # [0.43132667 0.43132667 0.43132667 ... 0.46391176 0.46783333 0.46783333]
    # [0.43132667 0.43132667 0.43132667 ... 0.46391176 0.46783333 0.46783333]
    # ==========================================================================
    # Utilizando o rgb2gray, o padrao da matrix é diferente.
    # Abre a img com o seguinte padrao:
    # [[110 110 110 ... 118 119 119]
    # [110 110 110 ... 118 119 119]
    # [110 110 110 ... 118 119 119]
    rgbImg = io.imread(path_img)
    grayImg = img_as_ubyte(color.rgb2gray(rgbImg))
    return return_gray_img

def img_8x8(gray_img):
    

    # ==========================================================================
    # O codio abaixo divide a imagem em blocos de 8x8 >>>PIXELS<<<. Deixarei
    #  o mesmo aqui para futuros textes extendendo as features de 64 partes da
    #  da imagem para nxm partes. Utilizando a forma abaixo, tem demorado 2hs
    #  para extrair as features necessárias.
    # ==========================================================================
    # Dividir cada imagem em 8x8 blocos
    # img = Image.open(path_img)
    # # TRANSFORMAR A IMAGEM EM CINZA
    # w, h = img.size # width, height da img
    # img = np.array(img)
    # sz = img.itemsize
    # shape = (h-bh+1, w-bw+1, bh, bw)
    # strides = (w*sz, sz, w*sz, sz)
    # return_blocks = np.lib.stride_tricks.as_strided(img, shape=shape, strides=strides)
    # return return_blocks


def extract_entropy(gray_img):
    ### FEATURE 1 ###
    # Extraindo a entropia da imagem inteira
    return_entropia = entropy(gray_img)
    return return_entropia

def entropia_por_bloco(list_blocos_img):
    # String de Retorno
    return_lista_entropia_img = []

    for i in range(len(list_blocos_img)):
        for j in range(len(list_blocos_img[i])):
            rgb_img = list_blocos_img[i][j]
            gray_img = img_as_ubyte(color.rgb2gray(rgb_img))
            return_lista_entropia_img.append(entropy(gray_img))
    return return_lista_entropia_img

def media_entropia_blocos(entropia_blocos):
    ### FEATURE 2 ###
    return_media_entropia = statistics.mean(entropia_blocos)
    return return_media_entropia

def stdev_entropia_blocos(entropia_blocos):
    ### FEATURE 3 ###
    return_stdev_entropia = statistics.stdev(entropia_blocos)
    return return_stdev_entropia

def markov_chains(lista_img):
    from itertools import chain
    import re

    # Variaveis
    chain_list = []
    lista_regex = []
    matrix_2x2_minima = ['0','0','1','1']
    return_markov_chain = []

    # Primeiro preciso converter em listas unidimensionais
    for i in range(len(lista_img)):
        for j in range(len(lista_img[i])):
            # Transformar em lista unidimesional
            chain_list.append(list(chain(*lista_img[i][j])))

    # Remover todos os caracteres que nao forem 0 e 1
    for i in range(len(chain_list)):
        # Regex sub substitui em "" oq nao for 0 ou 1 em casas unicas
        regex = re.sub('0*([1-9]\d+|[2-9])','',str(chain_list[i]))
        # Regex findall extrai apenas os numeros
        regex2 = re.findall('\d+', str(regex))
        lista_regex.append(regex2)

    # Adicionar 2 vezes 0 e 1 para formar a tabela 2x2
    for i in range(len(lista_regex)):
        for j in range(len(matrix_2x2_minima)):
            lista_regex[i].append(matrix_2x2_minima[j])

        # Aplicar a markov chains para cada posição
        mk = lista_regex[i]
        # A probabilidade (forward) do Estado Atual (0) para se Tornar um proximo
        #   Estado (1) pode ser encontrado na coluna '1' e linha '0' (0.5).
        #
        # Caso queira as probabilidades anteriores (backward), apenas sete o
        #       normalize = 1.
        #
        # === Exemplo do DATAFRAME para referencia da explicao acima ===
        #        Proximo    0    1
        #        Atual
        #        0        0.7  0.5
        #        1        0.0  1.0
        x = pd.crosstab(pd.Series(mk[:-1], name='Atual'),pd.Series(mk[1:],name='Proximo'),normalize=0)
        return_markov_chain.append(x)

    # Retornar markov_chains ou em dataframe (em array pode-se utilizar .values ao final)
    return return_markov_chain

def isolar_variaveis_matrix_markov_chains(lista_dataframe_mk):
    # Variaveis para append
    return_mk_00 = []
    return_mk_01 = []
    return_mk_10 = []
    return_mk_11 = []

    for i in range(len(lista_dataframe_mk)):
        x = 0
        # A cada linha eu faço o append para a lista de variaveis.
        for index, row in lista_dataframe_mk[i].iterrows():
            # Como é uma matrix fixa de 2x2, preferi usar um contador simples
            #       para a escolha de qual lista será usada para o append.
            if x == 0:
                    return_mk_00.append(row['0'])
                    return_mk_01.append(row['1'])
            else:
                    return_mk_10.append(row['0'])
                    return_mk_11.append(row['1'])
            x = x+1

    return return_mk_00, return_mk_01, return_mk_10, return_mk_11

def media_matrix_markov(input_mk_00,input_mk_01,input_mk_10,input_mk_11):
    ### FEATURE 4-7 ###
    return_media_00 = statistics.mean(input_mk_00)
    return_media_01 = statistics.mean(input_mk_01)
    return_media_10 = statistics.mean(input_mk_10)
    return_media_11 = statistics.mean(input_mk_11)

    return return_media_00, return_media_01, return_media_10, return_media_11

def stdev_matrix_markov(input_mk_00,input_mk_01,input_mk_10,input_mk_11):
    ### FEATURE 8-11 ###
    return_stdev_00 = statistics.stdev(input_mk_00)
    return_stdev_01 = statistics.stdev(input_mk_01)
    return_stdev_10 = statistics.stdev(input_mk_10)
    return_stdev_11 = statistics.stdev(input_mk_11)

    return return_stdev_00, return_stdev_01, return_stdev_10, return_stdev_11

def media_full_img(path_gray_img):
    # Variaveis
    markov_full_img = []
    lista_regex = []
    matrix_2x2_minima = ['0','0','1','1']

    for i in range(len(path_gray_img)):
        # Regex sub substitui em "" oq nao for 0 ou 1 em casas unicas
        regex = re.sub('0*([1-9]\d+|[2-9])','',str(path_gray_img[i]))
        # Regex findall extrai apenas os numeros
        regex2 = re.findall('\d+', str(regex))
        lista_regex.append(regex2)

    for i in range(len(lista_regex)):
        for j in range(len(matrix_2x2_minima)):
            lista_regex[i].append(matrix_2x2_minima[j])
        mk = lista_regex[i]
        x = pd.crosstab(pd.Series(mk[:-1], name='Atual'),pd.Series(mk[1:],name='Proximo'),normalize=0)
        markov_full_img.append(x)

    # Reutilizando as funções já definidas
    list_00, list_01, list_10, list_11 = isolar_variaveis_matrix_markov_chains(markov_full_img)
    r_full_00, r_full_01, r_full_10, r_full_11 = media_matrix_markov(list_00, list_01, list_10, list_11)

    return r_full_00, r_full_01, r_full_10, r_full_11

def main():
    # Calcular tempo inicial do script
    tempo_inicial = dt.datetime.now().strftime("%H:%M:%S")

    # Abrir a imagem e retornar em Gray Escale
    img_gray = open_img(argv_img)

    # Divide a Imagem em blocos de 8x8
    lista_img_8x8 = img_8x8(img_gray)

    # Extrai a entropia de cada bloco img_8x8
    lista_entropia_por_bloco = entropia_por_bloco(lista_img_8x8)

    # FEATURE 1 - Entropia da imagem inteira
    entropia_img_full = extract_entropy(img_gray)

    # FEATURE 2 - Media da entropia de cada bloco da imagem
    media_entropia = media_entropia_blocos(lista_entropia_por_bloco)

    # FEATURE 3 - Desvio Padrao da entropia de cada bloco da imagem
    stdev_entropia = stdev_entropia_blocos(lista_entropia_por_bloco)

    # Transformar cada bloco em Lista de Markov Chains
    matrix_markov_chains = markov_chains(lista_img_8x8)

    # Isolar variaveis do Dataframe para calculo de Média e Desvio Padrao
    mk_00, mk_01, mk_10, mk_11 = isolar_variaveis_matrix_markov_chains(matrix_markov_chains)

    # FEATURE 4-7 - Média de markov chains por cada bloco (0/0, 0/1, 1/0, 1/1).
    media_mk_00, media_mk_01, media_mk_10, media_mk_11 = media_matrix_markov(mk_00, mk_01, mk_10, mk_11)

    # FEATURE 8-11 - Desvio Padrao de markov chains por cada bloco (0/0, 0/1, 1/0, 1/1).
    stdev_mk_00, stdev_mk_01, stdev_mk_10, stdev_mk_11 = stdev_matrix_markov(mk_00, mk_01, mk_10, mk_11)

    # FEATURE 12-15 - "The average probability" por toda a imagem para cada uma das
    #                     transições (0/0, 0/1, 1/0, 1/1).
    media_full_00, media_full_01, media_full_10, media_full_11 = media_full_img(argv_img)

    entropy_dct_img =
    # FEATURE 16-51 - The conditional entropy for each non-boundary
    #                 position in the 8 x 8 DCT coefficient grid, calculated for
    #                 the entire image.
    # Aplicar entrpia para cada bloco DCT (dct cells 8x8)
    # Remover as DCT cells, separar em 8 listas e remover as primeira e a ultima, então a primeira e a ultima variavel
    print(entropia_img_full)
    print(media_entropia)
    print(stdev_entropia)
    print(media_mk_00)
    print(media_mk_01)
    print(media_mk_10)
    print(media_mk_11)
    print(stdev_mk_00)
    print(stdev_mk_01)
    print(stdev_mk_10)
    print(stdev_mk_11)
    print(media_full_00)
    print(media_full_01)
    print(media_full_10)
    print(media_full_11)

    print("--- Tempo Inicial de Execução:   "+ tempo_inicial +" ---")
    tempo_final = dt.datetime.now().strftime("%H:%M:%S")
    print("--- Tempo Final de Execução:     "+ tempo_final  + " ---")

if __name__ == "__main__":
        main()
